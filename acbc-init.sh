#!/bin/bash

# This script initializes the "acbc" (Analysis Chain for Bait Capture) pipeline, implemented
# on the HiPerGator cluster at the University of Florida.
#
# David Swofford, initial version 9 April 2018.

# Cleaning/adapter-trimming:
module load trim_galore
module load cutadapt

# 'variant' arg should be "HybPiper", "aTRAM", or "IBA"
variant="HybPiper"

if [ "$variant" = "IBA" ]; then
	echo "Initializing for IBA"
	module load usearch
	module load bridger
	module load python/2.7.14
	module load parallel
elif [ "$variant" = "aTRAM" ]; then
	echo "Initializing for aTRAM"
	module load atram/2.0
elif [ "$variant" = "HybPiper" ]; then
	echo "Initializing for HypPiper"
	module load ncbi_blast
	module load exonerate
	module load parallel
	module load spades
	module load bwa
	module load samtools
fi
