##Notes for Matt

* `WARNING: Potential paralogs detected for 0 genes` sounds like something went wrong.  Why call it a "warning" if everything is fine?
* Likewise, I find the message `[DISTRIBUTE]: 0 proteins had no good matches.` very confusing.
* Commenting out code temporarily is fine, but IMHO commented-out code should *never* be commited to a public branch, especially master.  Trying to modify/debug a file that contains a lot of commented-out code is a pain if you're not the one that knows why it's commented out.  Among other things, it makes it hard for others working on the code to temporarily comment things out for their own experimentation, as they have to add their comments about what was originally vs. newly commented out.   There are other reasons to avoid this practice as well; e.g., see:
	* <https://www.nayuki.io/page/dont-share-commented-out-code>
	* <https://markhneedham.com/blog/2009/01/17/the-danger-of-commenting-out-code/>
	* <https://blog.submain.com/commented-out-code-junk-codebase/>
* Maybe it's just me, but I find all the exclamation points after normal output status messages to be off-putting.  It reminds me of email messages beginning with "Congratulations!  You've been selected to apply for our credit card!!"
* Your files have many lines that containing trailing whitespace (including blank lines with spaces).  I would try to change your editor settings so that any trailing whitespace is always removed when a file is saved.   When I'm working on your repo, I have to tell git to ignore whitespace to avoid it thinking there are differences that are just due to whitespace, which isn't always a good idea especially for Python programs.