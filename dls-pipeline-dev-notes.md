## <center>DLS notes on modifications to "pata" and Breinholt pipelines</center>

### Modications to pata:
* pata checked for unmatched "R1" file (no "R2" equivalent) but not the other way around.
  I now also check for "R2" files that have no equivalent "R1" file.
* It is no longer assumed that all fastq files are in the same directory--eventually will allow assemblies
  to be performed on files in various directories, without needing them to copy them into the same
  directory
