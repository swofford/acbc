## ACBC--Analysis Chain for Bait Capture

This is a set of scripts and programs for assembly, alignment, and phylogenetic analysis of data obtained by relaxed-hybridization targeted capture (Li et al. 2013), as implemented on the HiPerGator cluster at the University of Florida.

*Below is out-of-date*

ACBC was developed by Dave Swofford for the analysis of elasmobranch (sharks and rays) data generated in the Gavin Naylor lab at UF.  The upstream parts of the pipeline are based heavily on the pipeline developed by Jesse Breinholt, Chandra Earl and their collaborators (Breinholt et al. 2018).  You are welcome to modify and use the scripts however you like.

#### Running the assembly pipeline interactively:

1. Create a file containing a list of all fastq files to be assembled.  Wild-card characters may be used to match multiple files.  Lines beginning with a pound character (#) are ignored.

2. Switch to a compute node.  E.g., I run a script called **qrsh** containing the following lines:

	```
	#!/bin/sh
	module load ufrc
	srundev --time=4:00:00 --ntasks=1 --cpus-per-task=4 --mem=4gb
	```

3. Run the acbc-init script to load necessary software modules and set environment variables:<br><br>
 ``
 source /ufrc/naylor/orange/dave/acbc/acbc-init.sh
 ``

4. Start the run:

	```
	/ufrc/naylor/orange/dave/acbc/acbc-pata.py seqlist.txt
	```

#### References:

* Breinholt J.W., Earl C., Lemmon A.R., Moriarty Lemmon E., Xiao L., and Kawahara A.Y. 2018. Resolving relationships among the megadiverse butterflies and moths with a novel pipeline for anchored phylogenomics. Systematic Biology 67:78–93.

* Li C., Hofreiter M., Straube N., Corrigan S., and Naylor G.J.P. 2013. Capturing protein-coding genes across highly divergent species. BioTechniques 54:321–326.
