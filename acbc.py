#!/usr/bin/env python
#
# ACBC (Analysis Chain for Bait Capture)
#
# Component of the ACBC pipeline for trimming of raw reads and subsequent assembly.
#
# This script has been tested under both Python 2.7.14 and Python 3.6.5 (switchable by changing
# the first line between "python" and "python3")
#
from __future__ import print_function
import os, sys, subprocess
import re
import glob
import argparse
import errno
import tempfile
import gzip
import shutil
import signal
from collections import namedtuple

#--------------------------------------------------------------------------------------------------#
#                                   Begin configuration section                                    #
#--------------------------------------------------------------------------------------------------#

# 'adapter1' and 'adapter2' are the Illumina adaptor sequences for each read direction:
adapter1, adapter2 = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC', 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT'

# Choose assembly method from "abyss", "trinity", "velvet", or "spades" ("none" for no assembly):
assembler = "spades"

# Choose a pipeline by setting exactly one of 'using_atram' or 'using_hybpiper' to True:
using_atram = False
using_hybpiper = True

# Directories to receive output trimmed sequences, trimming reports, and aTRAM intermediate files:
root_dir = "/ufrc/naylor/davidswofford/test"
trimmed_seqs_dir = root_dir + "/trimmed_sequences"
trimming_reports_dir = root_dir + "/trimming_reports"
assemblies_dir = root_dir + "/assemblies"
assembler_output_dir = assemblies_dir + "/" + assembler
if using_atram:
	atram_dir = root_dir + "/atram_files"

# 'fastq_mask' is a regular-expression string that a file in the input list must match before it is
# considered eligible as an input FASTQ file.  We assume that all reads are obtained from paired-
# end sequencing, and the 'r1_mask' and 'r2_mask' masks select the first and second member of the
# pair, respectively.
fastq_mask = '^.*_R[12](_001)?\.(fq|fastq)'
r1_mask = '^.*_R1$'
r2_mask = '^.*_R2$'

# Script used to initialize environment (currently only used for error messages):
init_file_name = "acbc-init.sh"

# Executable names (either full paths, or filenames assumed to be in user's path):
trim_galore_bin = 'trim_galore'

### TODO: make some/all of the settings below command-line switchable.

# Options for dealing with gzipped files:
#	If 'unzip_to_temp_dir' is set to True, input FASTQ files will be unzipped to a temporary
#	directory prior to further processing (e.g., aTRAM will not read gzipped files).  However,
#	TrimGalore will read the gzipped files, so this option would only be needed if TrimGalore is
#	not being used.  Ordinarily, the temporary directory and its contents will be deleted after
#	the run finishes; 'keep_temp_dir' can be set to True to suppress this deletion.  If
#	'gzip_trimmed_files' is True, output files from TrimGalore will be gzip-compressed (assumes
#	that downstream will deal with the files in that format).
unzip_to_temp_dir = False
keep_temp_dir = True   ### need to copy out assemblies before delete
gzip_trimmed_files = False

# If skip_trimming is True, assume that trimming has already been performed, and that trimmed
# sequences are in 'trimmed_seqs_dir' with an extension of ".fq".  Otherwise, trimming is performed
# using TrimGalore.
skip_trimming = False

# Options for trim_galore:
#	The "--trim1" option is used to request removal of one base from the 3' end of each pair of
#	reads.  This step is included to work around an issue in the original Bowtie where reads were
#	not allowed to completely contain each other.  The old pipeline used this option, but it seems
#	to me that this just discards data needlessly.
### trim1 = False
trim1 = True

#--------------------------------------------------------------------------------------------------#
#                                    End configuration section                                     #
#--------------------------------------------------------------------------------------------------#

# TODO: Eventually use a similar map to the assembled filenames, and don't do the assembly if it
#       has already been done.

def showFileList(file_list, header):
	print(header)
	for f in file_list:
		print("    %s" % f)

def createDirectoryIfNotPresent(path, die_if_unsuccessful):
	"""Creates the directory 'path'.  If it already exists, do nothing."""
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			if die_if_unsuccessful:
				sys.stderr.write("\nError: attempt to create directory '%s' failed.\n" % path)
				sys.exit(1)
			else:
				raise

def createTempDir(suffix_string):
	"""Creates a temporary directory with suffix 'suffix_string' (we assume this will succeed;
	if it fails, the program is simply terminated).
	"""
	try:
		return tempfile.mkdtemp(suffix=suffix_string)
	except:
		print("Could not create temporary directory")
		sys.exit(1)


def unzipFileToTempDir(zipped_pathname, outfile_pathname):
	"""Expands the file 'zipped_pathname' (assumed to be in gzip format), creating a new file
	'outfile_pathname'.
	"""
	with gzip.open(zipped_pathname, 'rb') as infile:
		content = infile.read()
		with open(outfile_pathname, 'wb') as outfile:
			outfile.write(content)

def exec_command(cmd):
	print("cmd =", cmd)  ### TEMP

	try:
		rc = subprocess.call(cmd)
	except OSError as e:
		if e.errno == os.errno.ENOENT:
			sys.stderr.write("\nError: command '%s' not found.  Make sure the environment has " \
    	                     "been initialized ('source %s')." % (cmd[0], init_file_name))
		else:
			sys.stderr.write("\nOSError %d: %s\n" % (e.errno, e.strerror))
		rc = 1
	except:
		print ("Error:", sys.exc_info()[0])
		rc = 1

	return rc

def run(args):

#	Read command-line arguments
	print
	parser = argparse.ArgumentParser(usage="%(prog)s seqlistfile [--help] [options...]")
	parser.add_argument( "seqlistfile", help="name of file containing list of fastq sequence " \
	                                         "files to use")
	parser.add_argument( "--dryrun", action='store_true', help="only show files to be used; "\
	                                                           "do not perform any analyses")
	try:
		args = parser.parse_args()
	except:
		print("\nHelp for %s:\n" % os.path.basename(sys.argv[0]))
		parser.print_help()
		sys.exit(1)

#	Open 'seqlistfile' file containing fastq sequence files to be included in the run.  Each line
#   in the file is either a single filename or a wildcard matching multiple files (e.g.,
#	"/some/directory/*" matches all of the files in /some/directory).  Lines beginning with '#'
#	are ignored.
	try:
		seqfile = open(args.seqlistfile, 'r')
	except:
		print("Could not read file:", args.seqlistfile)
		sys.exit(1)

	print("\nRunning %s using seqlistfile '%s'..." % (os.path.basename(sys.argv[0]),
													  args.seqlistfile))

#	Create directories for output sequence files, trimming reports, and assemblies.
	createDirectoryIfNotPresent(trimmed_seqs_dir, True)
	createDirectoryIfNotPresent(trimming_reports_dir, True)
	createDirectoryIfNotPresent(assemblies_dir, True)
	createDirectoryIfNotPresent(assembler_output_dir, True)
	temp_dir = createTempDir(".acbc")
	if using_atram:
		createDirectoryIfNotPresent(atram_dir, True)

#	Match all files specified in the filename list.
	file_list = []
	nonmatching_files = []
	re_fastq_match = re.compile(fastq_mask)
	for s in seqfile:
		files = glob.iglob(s.rstrip())
		for f in files:
			if f[0] != '#':
				if re_fastq_match.search(f):
					abs_path = os.path.abspath(f)
					file_list.append(abs_path)
				else:
					nonmatching_files.append(f)

#	Unzip any gzipped files to a new temporary directory and create dictionary of absolute paths.
	selected_files = {}
	max_path_len = 0
	max_name_len = 0
	for f in file_list:
		seqname, ext = os.path.splitext(os.path.basename(f))
		if ext.lower() == ".gz":
			if unzip_to_temp_dir:
				seqname, ext = os.path.splitext(os.path.basename(seqname))
				new_path = temp_dir + '/' + seqname + ext
				unzipFileToTempDir(f, new_path)
				f = new_path
			else:
				seqname = os.path.splitext(seqname)[0]
		selected_files[seqname] = f
		max_path_len = max(len(f), max_path_len)
		max_name_len = max(len(seqname), max_name_len)

#	Second pass: Find paired files.
	fastqPairs = []
	unpaired = []
	print("\nSelected FASTQ files:\n")
	print("    %-*s    Full path" % (max_name_len, "Name"))
	print("    " + '-'*max_name_len + "----" + '-'*max_path_len)
	for f in sorted(selected_files):
		fullPath = selected_files[f]
		print("    %-*s    %s" % (max_name_len, f, fullPath))
		if re.match(r1_mask, f) != None:
			f2 = re.sub(r'(.+)_R1', r'\1_R2', string=f)
			if f2 in selected_files:
				max_name_len = max(len(os.path.basename(f)), max_name_len)
				fastqPairs.append((f, f2))
			else:
				unpaired.append(fullPath)
		elif re.match(r2_mask, f) != None:
			f1 = re.sub(r'(.+)_R2', r'\1_R1', string=f)
			if f1 not in selected_files:
				unpaired.append(fullPath)

	if len(fastqPairs) == 0:
		sys.stderr.write("\nNo matching FASTQ pairs were found.\n")
		sys.exit(2)

	if nonmatching_files:
		showFileList(nonmatching_files, "\nThe following non-FASTQ file(s) were ignored:")

	print("\nInput FASTQ pairs:")
	for f1, f2 in fastqPairs:
		print("    %-*s  :  %s" % (max_name_len, f1, f2))
		f1_zipped = selected_files[f1][-3:] == ".gz"
		f2_zipped = selected_files[f2][-3:] == ".gz"
		if f1_zipped != f2_zipped:
			sys.stderr.write("\nError: Input file pairs must both be gzipped if either is " \
			                 "gzipped:\n  %s\n  %s\n" % (selected_files[f1], selected_files[f2]))
			sys.exit(2)

	if unpaired:
		showFileList(unpaired, "\nWARNING: Unpaired fastq files were found; these will be ignored:")

#	Get number of SLURM tasks available (will be 1 if not running in SLURM environment)
	ntasks = int(os.getenv('SLURM_NTASKS', 1))
	cpus_per_task = int(os.getenv('SLURM_CPUS_PER_TASK', 1))
	ncpus = ntasks*cpus_per_task
	print("\nStarting ACBC pipeline:")
	print("    ntasks = %d" % ntasks)
	print("    cpus_per_task = %d" % cpus_per_task)
	print("    total number of cpus available = %d" % ncpus)

	if args.dryrun:
		sys.exit(0)

	# Don't use Python-installed SIGPIPE handler (avoids "broken pipe" gzip message)
	signal.signal(signal.SIGPIPE, signal.SIG_DFL)

	gzip_setting = "--gzip" if gzip_trimmed_files else "--dont_gzip"
	rc = 0
	for f1, f2 in fastqPairs:

		f1_path = selected_files[f1]
		f2_path = selected_files[f2]
		f1_base = trimmed_seqs_dir + '/' + f1
		f2_base = trimmed_seqs_dir + '/' + f2
		f1_seqFile = f1_base + ".fq"
		f2_seqFile = f2_base + ".fq"

#		Run TrimGalore.
		cmd = [trim_galore_bin, "-a", adapter1, "-a2", adapter2, "--phred33", "--length", "20",
		       "--paired", gzip_setting, "--output_dir", trimmed_seqs_dir]
		if trim1:
			cmd.append("--trim1")
		cmd.append(f1_path)
		cmd.append(f2_path)
		if not skip_trimming:
			print("\nCalling %s for sequence pair (%s, %s)...\n" % (trim_galore_bin, f1, f2))
			rc = exec_command(cmd)
			if rc != 0:
				break

#			Rename trimmed sequences (dropping the "_val_1")
			os.rename(f1_base + "_val_1.fq", f1_seqFile)
			os.rename(f2_base + "_val_2.fq", f2_seqFile)

#			Move trimming reports to a separate directory.
			if not skip_trimming:
				tr1 = trimmed_seqs_dir + '/' + os.path.basename(f1_path) + "_trimming_report.txt"
				tr2 = trimmed_seqs_dir + '/' + os.path.basename(f2_path) + "_trimming_report.txt"
				os.rename(tr1, trimming_reports_dir + '/' + f1 + "_trimming_report.txt")
				os.rename(tr2, trimming_reports_dir + '/' + f2 + "_trimming_report.txt")

		name = f1[0:-3]
		print("-------- name = '%s'" % name)
		print("         f1_seqFile = '%s'" % f1_seqFile)
		print("         f2_seqFile = '%s'" % f2_seqFile)

		if using_atram:

			os.chdir(atram_dir)

###			consider putting files for each locus in separate directories
###			createDirectoryIfNotPresent(name, True)

			if 1: ### TEMP DEL
				cmd = ["atram_preprocessor.py",
						 "--blast-db",			name,
						 "--end-1",				f1_seqFile,
						 "--end-2",				f2_seqFile
					  ]
				if exec_command(cmd) != 0:
					break

			if 1: ### TEMP DEL
				cmd = ["atram.py"
				       , "--blast-db",			name,
#				       , "--query-split", 		"/ufrc/naylor/orange/dave/shark-phylogenomics/exon-reference-sequences/sharks_1075_reference_sequences.fasta"
				       , "--query-split", 		root_dir + "/test-ref-seq_consensus.fasta"
				       , "--protein"
				       , "--output-prefix",		assembler_output_dir + "/" + name
				       , "--cpus",				str(cpus_per_task)
				       , "--temp-dir",			temp_dir
				       , "--iterations",		"5"
				       , "--assembler",			assembler
#	 			       , "--evalue",			"1e-10"
				       , "--log-file",			"%s.Locus.log" % name
				       , "--sqlite-temp-dir",	temp_dir
				      ]
				if exec_command(cmd) != 0:
					break

			os.chdir("..")

		if using_hybpiper:

			cmd = ["../pipeline-development/software/HybPiper/reads_first.py"
#			       , "--check-depend"
			       , "--readfiles",			f1_seqFile, f2_seqFile
			       , "--cpu",				str(cpus_per_task)
#			       , "--baitfile", 			root_dir + "/test-ref-seq.fasta"
			       , "--baitfile", 			root_dir + "/test-ref-seq_exon4.fasta"
			       ###, "--cov_cutoff",		"off"	### ????????????????
			       , "--no-blast" ### TEMP
			      ]
			if exec_command(cmd) != 0:
				break

#	Clean up:
	if temp_dir != None and not keep_temp_dir:
		shutil.rmtree(temp_dir)

	return rc

if __name__ == '__main__':
	run(sys.argv)
