#!/usr/bin/env python
#
# ACBC-pata (Parallel automated trimming and assembly)
#
# Component of the ACBC pipeline for trimming of raw reads and subsequent assembly.
#
import os, sys, subprocess
import re
import glob
import argparse
import errno
from ntpath import basename

#----------------------------------------------------------------------------------------------------------------------#
# Begin configuration section                                                                                          #
#----------------------------------------------------------------------------------------------------------------------#

# 'fastq_mask1' and 'fastq_mask2' are regular-expression strings that files in the input list must match before
# they are considered eligible as input fastq files.  We assume that all reads are obtained from paired-end
# sequencing, in which case there is a different mask for each member of the pair.
fastq_mask1 = '^.*_R1(_001)?\.fastq\.gz$'
fastq_mask2 = '^.*_R2(_001)?\.fastq\.gz$'

# 'fastq_subst1' and 'fastq_subst2' are pairs of regular expressions for substituting the first member of a pair
# to the second or vice versa.
fastq_subst1 = r'(.+)_R1(.+)', r'\1_R2\2'
fastq_subst2 = r'(.+)_R2(.+)', r'\1_R1\2'

init_file_name = "acbc-init.sh"		# script used to initialize environment (only used for error messages)

#----------------------------------------------------------------------------------------------------------------------#
# End configuration section                                                                                            #
#----------------------------------------------------------------------------------------------------------------------#

# TODO: Eventually use a similar map to the assembled filenames, and don't do the assembly if it's already done.

def showFileList(file_list, header):
	print header
	for f in file_list:
		print "    %s" % f

def createDirectoryIfNotPresent(path):
	"""Creates the directory 'path'.  If it already exists, do nothing."""
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def run(args):

#	Read command-line arguments
	parser = argparse.ArgumentParser(usage="%(prog)s seqlistfile [--help] [options...]")
	parser.add_argument( "seqlistfile", help="name of file containing list of fastq sequence files to use" )
	parser.add_argument( "--dryrun", action='store_true', help="only show files to be used; "\
	                                                           "do not perform any analyses")
	try:
		args = parser.parse_args()
	except:
		print "\nHelp for %s:\n" % basename(sys.argv[0])
		parser.print_help()
		sys.exit(1)

#	Open 'seqlistfile' file containing fastq sequence files to be included in the run.  Each line in the file is
#	either a single filename or a wildcard matching multiple files (e.g., "/some/directory/*" matches all of the
#	files in /some/directory).  Lines beginning with '#' are ignored.
	try:
		seqfile = open(args.seqlistfile, 'r')
	except:
		print "Could not read file:", args.seqlistfile
		sys.exit(1)

#	Find paired fastq files matching the filename list and masks.
	pairs = []
	unpaired = []
	non_fastq = []
	max_name_len = 0
	for s in seqfile:
		files = glob.iglob(s.rstrip())
		for f in files:
			if f[0] != '#':
				if re.match(fastq_mask1, f):
					f2 = re.sub(*fastq_subst1, string=f)
					if os.path.isfile(f2):
						max_name_len = max(len(basename(f)), max_name_len)
						pairs.append((f, f2))
					else:
						unpaired.append(f)
				elif re.match(fastq_mask2, f):
					f1 = re.sub(*fastq_subst2, string=f)
					if not os.path.isfile(f1):
						unpaired.append(f)
				else:
					non_fastq.append(f)

#	Show files used (and ignored).
	if unpaired != []:
		showFileList(unpaired, "\nWARNING: Unpaired fastq files were found; these will be ignored:")
	if non_fastq != []:
		showFileList(non_fastq, "\nNote: The following non-fastq items will be ignored:")
	if len(pairs) == 0:
		sys.stderr.write("\nNo matching fastq pairs were found.\n")
		return(2)
	print "\nSequence file pairs used:"
	for p in pairs:
		print "    %-*s%s" % (max_name_len + 4, basename(p[0]), basename(p[1]))

#	Get number of SLURM tasks available (will be 1 if not running in SLURM environment)
	ntasks = int(os.getenv('SLURM_NTASKS', 1))
	cpus_per_task = int(os.getenv('SLURM_CPUS_PER_TASK', 1))
	ncpus = ntasks*cpus_per_task
	print "\nStarting ACBC pipeline:"
	print "    ntasks =", ntasks
	print "    cpus_per_task =", cpus_per_task
	print "    total number of cpus available =", ncpus

	if not args.dryrun:

		# Create these directories (if necessary) before we start the parallel tasks
		if 1:#####TEMP DEL
			createDirectoryIfNotPresent("trimming_reports")
			createDirectoryIfNotPresent("logs")

			###exit(0)####

			print "\nRunning %s using seqlistfile '%s'..." % (basename(sys.argv[0]), args.seqlistfile)
			assemble_bin = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'acbc-assemble.py')
			command = ['parallel', '--xapply', '-j', str(ncpus), assemble_bin, ':::']
			command += [pair[0] for pair in pairs]
			command.append(':::')
			command += [pair[1] for pair in pairs]
			#command = ['parallel', '--xapply', '-j', str(ncpus), "ls", "-l"]. ##### TEMP
			try:
				print "calling subprocess with command = '%s'" % command
				subprocess.call(command)
				print "back from subprocess (%s)" % command
			except:
				print "\nError: Attempt to run 'parallel %s' command failed.  Make sure environment has been"\
					  " initialized ('source %s')." % (assemble_bin, init_file_name)

if __name__ == '__main__':
	run(sys.argv)
