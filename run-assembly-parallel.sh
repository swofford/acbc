#!/bin/sh

#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=100mb
#SBATCH --job-name=ACBC_assembly
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=dswofford@floridamuseum.ufl.edu
#SBATCH --output=ACBC_assembly_%j.log

abcd_root=/ufrc/naylor/orange/dave/acbc
source $abcd_root/acbc-init.sh

cd /ufrc/naylor/orange/dave/test
$abcd_root/acbc-pata.py seqlist.txt
