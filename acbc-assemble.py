#!/usr/bin/env python

import os, sys
import re
import shutil
import subprocess
from ntpath import basename

# 'adapter1' and 'adapter2' are the Illumina adaptor sequences for each read direction
adapter1, adapter2 = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC', 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT'

# Names for scripts and executables.  Unless absolute paths are given, it is assumed that the directories containing
# these programs are included in your path.  On the HiPerGator cluster, you can simply make these programs available
# by sourcing the 'acbc-init.sh' script, which loads the necessary modules to obtain these commands.
trim_galore_bin = 'trim_galore'

abyss_bin = 'abyss-pe'

script_path = os.path.dirname(os.path.realpath(__file__))
trans_abyss_dir = os.path.join(script_path, 'software', 'trans-ABySS-v1.4.8')
trans_abyss_env = os.path.join(trans_abyss_dir, 'setup.sh')
trans_abyss_fem_bin = os.path.join(trans_abyss_dir,'wrappers/abyss-ta-filter')
trans_abyss_rmdups_bin = os.path.join(trans_abyss_dir, 'wrappers/abyss-rmdups-iterative')
print "trans_abyss_dir=",trans_abyss_dir ### TEMP
print "trans_abyss_rmdups_bin=",trans_abyss_rmdups_bin ### TEMP

print("system version =", sys.version)
### exit(0)

#trim_galore_bin = os.path.join(script_path, 'software', 'trim_galore_v0.3.1', 'trim_galore')

def trim_galore(working_directory, file1, file2, outfile):
#	command = '/usr/bin/perl ' + trim_galore_bin + ' -a {} -a2 {} --paired {} {}'.format(adapter1, adapter2, file1, file2)
	command = trim_galore_bin +  " -a %s -a2 %s --phred33 --length 20 --paired --trim1 %s %s" % (adapter1, adapter2, file1, file2)
	print('\'' + command + '\'')
	out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, cwd=working_directory)  ####DLS: cwd needed if working_directory = current directory?
	outfile.write(out)

	if os.path.exists("trimming_reports"):
		trimfile1 = basename(file1) + '_trimming_report.txt'
		trimfile2 = basename(file2) + '_trimming_report.txt'
		os.rename(trimfile1, "trimming_reports/" + trimfile1)
		os.rename(trimfile2, "trimming_reports/" + trimfile2)

	if os.path.splitext(file1)[1] == '.gz':
		new_file1 = os.path.splitext(os.path.splitext(file1)[0])[0] + '_val_1.fq.gz'
		new_file2 = os.path.splitext(os.path.splitext(file2)[0])[0] + '_val_2.fq.gz'
	else:
		new_file1 = os.path.splitext(file1)[0] + '_val_1.fq'
		new_file2 = os.path.splitext(file2)[0] + '_val_2.fq'

	return new_file1, new_file2

def abyss(working_directory, file1, file2, kmer, root_name, kmer_directory, outfile):
	command = 'source {}; {} -C {} s=150 n=5 k={} name={} in=\'../../{} ../../{}\''.format(trans_abyss_env, abyss_bin, kmer_directory, kmer, root_name, file1, file2)
	print "in abyss: command = '%s'" % command
	try:
		out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, cwd=working_directory)
		outfile.write(out)
	except subprocess.CalledProcessError as e:
		pass

def trans_abyss_fem(working_directory, file1, file2, kmer, root_name, kmer_directory, outfile):
	command = 'source {}; {} -i {} -k {} -n {} -o {} -l 151'.format(trans_abyss_env, trans_abyss_fem_bin, kmer_directory, kmer, root_name, kmer_directory)
	print(command)
	out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, cwd=working_directory)
	outfile.write(out)

def trans_abyss_rmdups(working_directory, ta_directory, root_name, outfile):
	command = trans_abyss_rmdups_bin + ' -i {} -n {} -o {}'.format(ta_directory, root_name, ta_directory)
	print(command)
	out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, cwd=working_directory)
	outfile.write(out)

def get_read_length(filename):
	print "DLS: in get_read_length with filename='%s'" % filename
	if os.path.splitext(filename)[1] == '.gz':
		import gzip
		file = gzip.open(filename)
	else:
		file = open(filename)
	line = file.readline()
	print "DLS: line 1 = '%s'" % line
	line = file.readline()
	print "DLS: line 2 = '%s'" % line
	print "returning len(line.strip()) =", len(line.strip())
	return len(line.strip())

def run(file1, file2):
##	working_directory = os.path.dirname(file1)
	working_directory = os.getcwd()
	#file1, file2 = os.path.basename(file1), os.path.basename(file2)

#	read_length = get_read_length(os.path.join(working_directory, file1))
	read_length = get_read_length(file1)
	root_name = os.path.basename(re.sub(r'(.+)(_S[0-9]+)(_L[0-9]+)_R1(_[0-9]+)?\.fastq(\.gz)?', r'\1', file1))
#	outfile = open(os.path.join(working_directory, root_name + '.log'), 'wb')
	outfile_name = root_name + '.log'
	outfile = open(outfile_name, 'wb')

	print('Assembling {}'.format(root_name))

	try:
		file1, file2 = trim_galore(working_directory, file1, file2, outfile)
	except subprocess.CalledProcessError as e:
		print('Error in trim_galore: ' + str(e.output))
		return
	print "trim_galore returns file1, file2 =", file1, ",", file2  ### TEMP


	if 1:	#### TEMP HIDE
		ta_directory = root_name + '_trans_abyss'
		if not os.path.isdir(os.path.join(working_directory, ta_directory)):
			os.mkdir(os.path.join(working_directory, ta_directory))

###		upper_bounds = {150 : 131, 250 : 201, 251 : 201, 300 : 251}
		upper_bounds = {150 : 131, 151 : 131, 250 : 201, 251 : 201, 300 : 251}
		try:
			upper_bound = upper_bounds[read_length]
		except:
			outfile.write('Error: unknown read length, {}, stopping assembly\n'.format(read_length))
			print('Error during assembly of {}: unknown read length, {}, stopping assembly'.format(root_name, read_length))
			return

		outfile.write('Detected read length: {}\n'.format(read_length).encode('utf8'))
		outfile.write('Assembling with k-mer in range 51 to {} (multiples of 10)\n'.format(upper_bound).encode('utf8'))

		for kmer in range(51, upper_bound + 1, 10):
			kmer_directory = os.path.join(ta_directory, 'k{}'.format(kmer))
			print "kmer_directory =", kmer_directory ### TEMP
			kmer_full_path = os.path.join(working_directory, kmer_directory)
			print "kmer_full_path =", kmer_full_path ### TEMP
			if not os.path.isdir(kmer_full_path):
				print "################### mkdir making dir", kmer_full_path ### TEMP
				try:
					os.mkdir(kmer_full_path)
				except:
					print "################## mkdir failed" ####TEMP
					exit(1)
			abyss(working_directory, file1, file2, kmer, root_name, kmer_directory, outfile)
			if not os.path.isfile(os.path.join(working_directory, kmer_directory, root_name + '-contigs.fa')):
				outfile.write('No contigs file created for k-mer {}, not attempting any higher k-mer\n'.format(kmer).encode('utf8'))
				shutil.rmtree(os.path.join(working_directory, kmer_directory))
				break
			trans_abyss_fem(working_directory, file1, file2, kmer, root_name, kmer_directory, outfile)
		trans_abyss_rmdups(working_directory, ta_directory, root_name, outfile)

		os.rename('{}/{}-contigs.fa'.format(os.path.join(working_directory, ta_directory), root_name), '{}/{}-contigs.fa'.format(working_directory, root_name))
		shutil.rmtree(os.path.join(working_directory, ta_directory))
		os.remove(os.path.join(working_directory, file1))
		os.remove(os.path.join(working_directory, file2))

	outfile.close()
	if os.path.exists("logs"):
		os.rename(outfile_name, "logs/" + outfile_name)


	print('Finished {}'.format(root_name))

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print("Usage: assemble.py file1 file2")
	else:
		run(sys.argv[1], sys.argv[2])
